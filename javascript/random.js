$(document).ready(function(){
	$("#randomBtn").click(function(){
		$("#randomModal").show();
		$("body").addClass("over");
	});

	$("#detailBtn").click(function(){
		$("#detailModal").show();
		$("body").addClass("over");
	});

	$("#randomClose").click(function(){
		$("#randomModal").hide();
		$("body").removeClass("over");
	});

	$("#detailClose").click(function(){
		$("#detailModal").hide();
		$("body").removeClass("over");
	});

	$("#cancel").click(function(){
		$("#detailModal").hide();
		$("body").removeClass("over");
	});

	$(".rcmd_img").click(function(){
		$("#detailModal").show();
		$("body").addClass("over");
	});

	$("#addTerm").click(function(){
		$("#addTermModal").show();
	});

	$(".editTerm").click(function(){
		$("#editTermModal").show();
	});

	$("#addTermClose").click(function(){
		$("#addTermModal").hide();
	});

	$("#addTermBtn").click(function(){
		$("#addTermModal").hide();
	});

	$("#editTermClose").click(function(){
		$("#editTermModal").hide();
	});

	$("#editTermBtn").click(function(){
		$("#editTermModal").hide();
	});
});


// var randomModal = document.getElementById("randomModal");

//var detailModal = document.getElementById("detailModal");

// var randomBtn = document.getElementById("randomBtn");

// var detailBtn = document.getElementById("detailBtn")

// var randomClose = document.getElementById("randomClose");

// var detailClose = document.getElementById("detailClose");

// var image = document.getElementsByClassName("rcmd_img");

// var cancel = document.getElementById("cancel");

// randomBtn.onclick = function() {
//     randomModal.style.display = "block";
// }

// detailBtn.onclick = function() {
//     detailModal.style.display = "block";
// }

// randomClose.onclick = function() {
//     randomModal.style.display = "none";
// }

// detailClose.onclick = function() {
//     detailModal.style.display = "none";
// }

// cancel.onclick = function() {
//     detailModal.style.display = "none";
// }

// for (var i = 0; i < image.length; i++) {
//     image[i].onclick = function() {
//         detailModal.style.display = "block";
//     }
// }
// window.onclick = function(event) {
//     if (event.target == randomModal) {
//     randomModal.style.display = "none";
//     }
//     if (event.target == detailModal) {
//     detailModal.style.display = "none";
//     }
// }