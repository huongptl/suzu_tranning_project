var name = "Suzu";
var num = 1;
var flag = true; //false
var arr = [1, 2, 3, 4, 5];
console.log(name);
console.log(num);
console.log(flag);
console.log(arr);

var a = 14;
var b = 20;
var cong = a + b;
var tru = a - b;
var nhan = a * b;
var chia = a / b;
var so_du = a % b;
console.log(cong);
console.log(tru);
console.log(nhan);
console.log(chia);
console.log(so_du);

// alert(++a);

var x = 12;
var y = 10;
console.log(x += y); //x = x + y
console.log(x -= y); //x = x - y
console.log(x *= y); //x = x * y
console.log(x /= y); //x = x / y

//Toán tử quan hệ: > < >= <= == !=

console.log(x > y);
console.log(x < y);
console.log(x >= y);
console.log(x <= y);
console.log(x == y);
console.log(x != y);

//Phân biệt == và ===
var m = 10;
var n = "10";
console.log(m == n); //so sanh gia tri
console.log(m === n); //so sanh ca gia tri va kieu du lieu

//Cau lenh dieu kien if
//if (condition) {
	//alert();}
// if (x < y) {
// 	alert("x co gia tri lon hon y");
// }
// else {
// 	alert("x co gia tri nho hon y");
// }

console.log("==========")
// var color = prompt("Input color: ")
// switch (color){
// 	case 'red' :
// 		console.log("Red. OK");
// 		break;
// 	case 'yellow' :
// 		console.log("yellow. OK");
// 		break;
// 	default :
// 		console.log("not found");
// }

function func_sum(){
	var var1 = 1;
	var var2 = 2;
	var sum = var1 + var2;
	return sum;
}
console.log(func_sum())

console.log("===========");

//vong lap for
for (var i = 0; i < 10; i++) {
	//if (i == 5) {
		//break; (lap den khi i=5 roi thoat khoi vong lap)
		//continue; (lap den het vong lap, nhung bo qua i=5)
	//}
	console.log(i);
}

//vong lap while
console.log("=====while=====");
var j = 1;
while (j <= 10) {
	console.log(j);
	j++;
}

//vong lap do while
console.log("=====do while=====");
var k = 11;
do {
	console.log(k);
	k++;
}while(k < 10)
//do while se in ra it nhat 1 vong lap truoc, roi moi kiem tra dieu kien

//jQuery
